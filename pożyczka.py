#!/usr/bin/python
# -*- coding: latin-1 -*-
import os, sys
print('Przykład spłaty kredytu')
loan = input('Wartość kredytu: ')
loan = int(loan)
interest = input('Wartość oprocentowania: ')
interest = float(interest)*0.01
year = input('Czas spłaty(w latach): ')
year = int(year)

year_in_month = year * 12

flat = (loan * (interest/12)*(1+(interest/12))**year_in_month)/(((1+(interest/12))**year_in_month )-1)
flat = round(flat,2)

message1 = 'Twoja stała rata to: {:f} zł na miesiąc'
print(message1.format(flat))

message2 = 'kwota kredytu to: {:d} zł'
print(message2.format(loan))

message3 = 'Twoja pozostała kwota kredytu to {:0}, to {:0} mniej niż w poprzednim miesiącu.'
message4 = 'Miesiąc: {:d}'

month = 1
while month <= 24:
    X = loan - flat
    print(message4.format(month))
    print(message3.format(X,flat))
    month += 1
    loan = X